---
author: Kevin R, Coombes
date: 2 November 2018
title: Comparison Between Methods to Normalize RNA-Seq Data
---

# Overview
This project holds material to perform analyses and write the resulting
papers comparing various methods to normalize RNA sequencing data.

# Structure
The project is organized using the principles enumerated in the
[Orgamizer GitLab project](https://gitlab.com/krcoombes/organizer).
