---
title: "Single Gene Analyses"
author: "Kevin R. Coombes"
date: "19 February 2019"
output:
  html_document:
    highlight: kate
    theme: yeti
    toc: true
    keep_md: true
---

```{r opts, echo=FALSE, results="hide"}
knitr:::opts_chunk$set(fig.path="../results/knitfigs/")
```

```{r mycss, results="asis", echo=FALSE}
cat('
<style type="text/css">
b, strong {color: red; }
i, em {color: blue; }
.defn {color: purple; }
.para {color: purple;
      font-weight: bold;
}
.figure { text-align: center; }
.caption { font-weight: bold; }
</style>
')
```

## Processed Data Sets

```{r paths}
source("00-paths.R")
```

```{r echo=FALSE,results="hide"}
paths <- lapply(paths, function(P) sub("G", "H", P)) # temp moved on home machine
```

```{r vers}
f <- file.path(paths$scratch, "normedData.rda")
if (file.exists(f)) {
  load(f)
} else {
  vers <- dir(paths$clean)[c(5, 7, 1, 4, 6, 8)]
  names(vers) <- c("Raw", "TMM", "DESeq", "Quant", "RPKM", "TPM")
  normedData <- list()
  for (V in vers) {
    load(file.path(paths$clean, V, "GSE47774_ILM_AGR.Rda"))
    normedData[[V]] <- dataset
    rm(dataset)
  }
save(vers, normedData, file = f)
}
rm(f)
vers

```

## Gene Annotations
We will use the BioConductor <tt>Homo.sapiens</tt> database pacakage to get gene
annotations.
```{r Hs}
if (!require("Homo.sapiens")) {
  source("https://bioconductor.org/biocLite.R")
  biocLite("Homo.sapiens")
  library(Homo.sapiens)
}
```

Now we are going to use the `Homo.sapiens` BioCOnductror pacakge to obtain gene
symbols from the RefSeq IDs that are used as row names in the data sets. We also
work to compute gene lengths by summing the lengths of the associated exons.
```{r biglist}
f <- file.path(paths$scratch, "biglist.rda")
if (file.exists(f)) {
  load(f)
} else {
  biglist <- select(Homo.sapiens, rownames(normedData[["Raw"]]), keytype="REFSEQ",
                    columns=c("SYMBOL", "GENEID", "MAP"))
  foo <- select(Homo.sapiens, rownames(normedData[["Raw"]]), keytype="REFSEQ",
                columns=c("SYMBOL", "GENEID", "MAP", "EXONID", "EXONSTART", "EXONEND"))
  foo$EXONLEN <- foo$EXONEND - foo$EXONSTART + 1
  gl <- tapply(foo$EXONLEN, list(foo$REFSEQ), sum)
  barf <- gl[biglist$REFSEQ]
  barf[is.na(barf)] <- 1000 # lie when there's no data
  biglist$GENELENGTH <- barf
  rm(foo, barf, gl)
  save(biglist, file = f)
}
rm(f)
```

Both of the calls to the Hom spaiens data base returned mutliple matches for some RefSeq
IDs. We're going to take the easy way out (i.e., cheat) and just keep the first match.
```{r symbols}
f <- file.path(paths$scratch, "cheatGenes.rda")
if (file.exists(f)) {
  load(f)
} else {
  biglist$REFSEQ <- factor(biglist$REFSEQ)
  biglist$SYMBOL <-factor(biglist$SYMBOL)
  tab <- table(biglist$REFSEQ)
  doubled <- biglist[biglist$REFSEQ %in% names(tab)[tab==2],]
  cheatGenes <- biglist[!duplicated(biglist$REFSEQ),]
  rownames(cheatGenes) <- as.character(cheatGenes$REFSEQ)
  save(cheatGenes, file = f)
}
rm(f)
dim(biglist)
dim(cheatGenes)
all(rownames(cheatGenes) == rownames(normedData[["Raw"]]))
```


## Sample Genes
We want to have a few genes around to see how various methods affect them. We will
use this function to simplify making multiple versions of similar plots.
```{r myplot}
samtype <- factor(substring(colnames(normedData[["Raw"]]),14,14), 
                  levels=c("A", "C", "Z", "D", "B"))
pseudox <- (-1+as.numeric(samtype))*64 + rep(1:64, times=4)
myplot <- function(x, ...) {
  plot(pseudox, x, pch=16, xaxt='n', xlab="Sample", ...)
  abline(v=seq(0, 288, 64)+0.5)
  mtext(levels(samtype), side=1, line=1, at=c(32, 96, -100, 224, 288))
}
```

One of our favorite genes is TP53.
```{r tp53}
N <- normedData[["Raw"]]
tp53 <- N[tp53.dex <- grep("^TP53$", cheatGenes$SYMBOL)[1],]
myplot(tp53, main="TP53")
```

We're going to use te gene CD59 simply because we don't think there is anything at all special about it in this data set.
```{r cd59}
cd59 <- normedData[["Raw"]][cd59.dex <- grep("^CD59$", cheatGenes$SYMBOL)[1],]
myplot(cd59, main="CD59")
```

Next, we look at RNA Polymerase II, which is sometimes claimed to be a housekeeping gene.
```{r polr2a}
polr2a <- normedData[["Raw"]][polr2a.dex <- grep("^POLR2A", cheatGenes$SYMBOL),]
myplot(polr2a, main="POLR2A")
```

Another putative housekeeping gene is GAPDH, though we have never been convinced by this claim
```{r gapdh}
gapdh <- normedData[["Raw"]][gapdh.dex <- grep("^GAPDH$", cheatGenes$SYMBOL)[1],]
myplot(gapdh, main="GAPDH")
```


```{r rawbox}
if (!require(Polychrome)) {
  install.packages("Polychrome")
  library(Polychrome)
}
p36 <- palette36.colors(36)
mycols <- p36[c(A=3, C=23, Z=2, D=33, B=36)]
names(mycols) <- levels(samtype)
colmset <- seq(3, 256, 8)
colorvec <- mycols[samtype[colmset]]
boxplot(log2(1 +normedData[["Raw"]])[, colmset], col=colorvec, main="Raw Data")
```

## Figures Redux

```{r my6plot}
my6plot <- function(GENE) {
  geneIndex <- which(cheatGenes$SYMBOL == GENE)
  if (length(geneIndex) == 0) {
    stop("Cannot find gene '", GENE, "'") 
  }
  if (length(geneIndex) > 1) {
    warning("Multiple matches for gene '", GENE, "'; using the first one.")
    geneIndex <- geneIndex[1]
  }
  opar <- par(mfrow=c(2,3))
  on.exit(par(opar))
  for (shortv in names(vers)) {
    V <- vers[shortv]
    values <- normedData[[V]][geneIndex,]
    tag <- paste(GENE, shortv, sep=", ")
    myplot(values, main = tag)
  }
}
```


```{r tp53.plots, fig.width=12, fig.height=8}
my6plot("TP53")
```

```{r cd59.plots, fig.width=12, fig.height=8}
my6plot("CD59")
```

```{r polr2a.plots, fig.width=12, fig.height=8}
my6plot("POLR2A")
```

```{r gapdh.plots, fig.width=12, fig.height=8}
my6plot("GAPDH")
```

## "Housekeeping"" Genes
See [Genomics Online](https://www.genomics-online.com/resources/16/5049/housekeeping-genes/)

```{r actb.plots, fig.width=12, fig.height=8}
my6plot("ACTB")
```

```{r pgk1.plots, fig.width=12, fig.height=8}
my6plot("PGK1")
```

```{r rpl13a.plots, fig.width=12, fig.height=8}
my6plot("RPL13A")
```

```{r gusb.plots, fig.width=12, fig.height=8}
my6plot("GUSB")
```

```{r b2m.plots, fig.width=12, fig.height=8}
my6plot("B2M")
```

```{r tfrc.plots, fig.width=12, fig.height=8}
my6plot("TFRC")
```

Could get more at [List of human housekeeping genes](https://m.tau.ac.il/~elieis/HKG/)

## Glioblastoma Genes

References:

1. [Hoelzinger et al](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC1490313/)
2. [Wang A, Zhang G](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5661398/)

```{r idh1.plots, fig.width=12, fig.height=8}
my6plot("IDH1")
```

```{r egfr.plots, fig.width=12, fig.height=8}
my6plot("EGFR")
```

```{r nf1.plots, fig.width=12, fig.height=8}
my6plot("NF1")
```

```{r pik3ca.plots, fig.width=12, fig.height=8}
my6plot("PIK3CA")
```

```{r pten.plots, fig.width=12, fig.height=8}
my6plot("PTEN")
```

```{r rb1.plots, fig.width=12, fig.height=8}
my6plot("RB1")
```

```{r atx.plots, fig.width=12, fig.height=8}
my6plot("ENPP2")
```

```{r bcll2.plots, fig.width=12, fig.height=8}
my6plot("BCL2L2")
```

```{r igfbp2.plots, fig.width=12, fig.height=8}
my6plot("IGFBP2")
```

```{r calm3.plots, fig.width=12, fig.height=8}
my6plot("CALM3")
```

# Appendix
This analysis was performed in the following directory.
```{r where}
getwd()
```
This analysis was performed using the following R packages.
```{r si}
sessionInfo()
```
