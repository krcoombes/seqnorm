#***************************************************
# RNA-Seq Normalization Techniques
# note: ILM_NYG (Illumina, New York Genomics) was not included in analysis
# due to going rogue by using gene symbols opposed to refseq IDs.
#
# Be sure to read each section header before running each section
# as some sections were only used in the initial writing of the script.
# If you are running the scripts be sure that the directories are in the
# correct layout. If the directories and filenames are not in the same layout,
# the code will have to be changed to import the files from an alternate directory.
#***************************************************

#***************************************************
# iii) If below packages (edgeR, DESeq, preprocessCore) are not
# installed, please uncomment the next six lines and run to install.
if (!require("edgeR")) {
  source("http://bioconductor.org/biocLite.R")
  biocLite("edgeR")
}
if (!require("DESeq2")) {
  source("https://bioconductor.org/biocLite.R")
  biocLite("DESeq2")
}
if (!require("DESeq")) {
  source("http://bioconductor.org/biocLite.R")
  biocLite("DESeq")
}
if (!require("preprocessCore")) {
  source("http://bioconductor.org/biocLite.R")
  biocLite("preprocessCore")
}
if (!require("ClassComparison")) {
  source("http://silicovore.com/OOMPA/oompaLite.R")
  oompaLite() # also includes ClassDiscovery
}
if (!require("biomaRt")) {
  source("http://bioconductor.org/biocLite.R")
  biocLite("biomaRt")
}
if (!require("Biostrings")) {
  source("https://bioconductor.org/biocLite.R")
  biocLite("Biostrings")
}
#***************************************************
### added by KRC

### write out the location in exactly one place, which makes it easier to
### edit if you move the source files
root <- "~/Desktop/SEQC_norm/qPCR_RNAseq_data/GSE47774_"
gse <- "/GSE47774_"

### code that has long lists of these items is really hard to read.
### this provides structure and simplifies all the later code.
ilm <- c("ILM_AGR","ILM_BGI","ILM_CNL","ILM_COH","ILM_MAY","ILM_NVS")
lif <- c("LIF_LIV","LIF_NWU","LIF_PSU","LIF_SQW")
roc <- c("ROC_MGP","ROC_NYU","ROC_SQW")
locations <- c(ilm, lif, roc)
ilmr <- c(ilm, paste(ilm, "rand", sep="_"))
lifr <- c(lif, paste(lif, "rand", sep="_"))
rocr <- c(roc, paste(roc, "rand", sep="_"))
ilmp <- c("ILM_AGR_pois")
randvers <- paste(locations, "rand", sep="_")
datasets <- c(locations, randvers, ilmp)
datasets_noROC <- c(ilmr,lifr,ilmp)
samples = c("A","B","C","D");
errors = c("Yerr","Xerr","Perr");
methodlist <- c("Raw","TMM-3.14.0","DESeq-1.24.0","quant-1.34.0","RPKM",
                "TPM","Log2");

### probably don't need this since we are changing our measure of linearity.
### But it is infinitely faster than a for-loop calling 'cor.test'
quickCor <- function(M, V) {
  # M is a matrix, V is a vector
  if (length(V) != ncol(M)) {
    stop("sizes do not match.")
  }
  # standardize everything
  sv <- as.matrix(scale(V), ncol=1)
  sm <- t(scale(t(M))) # need transpose to scale rows=genes
  as.vector((sm %*% sv)/(ncol(M) - 1))
}

# figure out which samples are type (A, B, C, D) automagically
# from the file names (which become column names)
extractDesign <- function(dset, MAGIC=14) {
  samp <- substring(colnames(agr), MAGIC, MAGIC)
  fsamp <- factor(samp)
  theory <- c(A=0, C=0.25, D=0.75, B=1)
  xx <- theory[samp]
  data.frame(xx=xx, fsamp=fsamp)
}

extractDesign <- function(dset) {
  for (j in 1:ncol(mat)){
    tmp[j] = strsplit(colnames(mat)[j],"_")[[1]][4]
  }
  samp <- substring(colnames(agr))
  fsamp <- factor(samp)
  theory <- c(A=0, C=0.25, D=0.75, B=1)
  xx <- theory[samp]
  data.frame(xx=xx, fsamp=fsamp)
}

library(ClassComparison)
ourLinearityMeasure <- function(dset) {
  covars <- extractDesign(dset) # assume that MAGIC works
  # fit a model with separate means for four groups (A, B, C, D)
  mlm4 <-  MultiLinearModel(Y ~ fsamp, covars, dset)
  # fit a linear model as a function of (0, 1/4, 3/4, 1)
  mlm0 <- MultiLinearModel(Y ~ xx, covars, dset)
  # analysis of variance
  cross <- anova(mlm0, mlm4)
  # fix the missing p-values from the ANOVA F-tests
  missing <- is.na(cross$p.values)
  cross$p.values[missing] <- 0.5
  # return the mean of the negative (natural)log p-values
  # note that expected value under the null hypothesis equals 1.
  # values bigger than one show deviations from linearity
  mean(-log(cross$p.values))
}

if (FALSE) { # don't need to run every time
  # show that expected value of 'ourLinearirityMeasure' is 1
  expected <- sapply(1:100, function(i) { # i is ignored
    r <- runif(43919)
    mean(-log(r))
  })
  summary(expected)
}

#*********************************************************************
### Added by TSJ 11/18/16
### Calculating gene lengths and matching to the RNA-Seq datasets
library("Biostrings")

x = read.table(paste0(root,"Raw/GSE47774_ILM_AGR.txt"), header=T, sep="\t", row.names = 1)
gnames.RNAseq = row.names(x); rm(x);
rna_fasta = readDNAStringSet("~/Desktop/SEQC_norm/rna.fa");
tmp = sapply(names(rna_fasta),function(x) strsplit(x,"|",fixed=TRUE));
rna_fasta_names = matrix(length(tmp),1);
# More efficient code below
for (i in 1:length(tmp)){
    rna_fasta_names[i] = substr(tmp[[i]][4],1,regexpr("\\.",tmp[[i]][4])[[1]][1]-1)
}
rm(tmp)
glens.RNAseq = matrix(length(gnames.RNAseq),1);
# More efficient code below
for (i in 1:length(gnames.RNAseq)){
  if(sum(gnames.RNAseq[i]==rna_fasta_names) > 0){
    glens.RNAseq[i] = width(rna_fasta[which(gnames.RNAseq[i]==rna_fasta_names)]);
  }else{
    glens.RNAseq[i] = NA;
  }
}
rm(rna_fasta); rm(rna_fasta_names);

#*********************************************************************
### Added by TSJ 12/15/16
### Importing ENSEMBL/RefSeq conversion biomart file
### downloaded from biomart Ensembl Attributes: "Transcript ID" and "RefSeq mRNA [e.g. NM_001195597]"
### Importing qpcr file and converting IDs to ReSeq
### returns indexes of the RNAseq rows corresponding to the qPCR rows
RNAseq = read.table(paste0(root,"Raw/GSE47774_ILM_AGR.txt"), header=T, sep="\t", row.names = 1)
conv_table = read.table('/Users/joh507/Desktop/SEQC_norm/mart_export.txt', header=T, sep=",")
qpcr = read.table('/Users/joh507/Desktop/SEQC_norm/GSE56457_SEQC_qPCR_GEOSub.txt', quote = "", header=T, sep='\t', row.names=1, fill=TRUE)

qpcr_refseq = matrix(ncol=5,nrow=dim(qpcr)[1]);
for (i in 1:dim(qpcr)[1]){
  if (length(which(conv_table[,1]==as.character(qpcr[i,3]))) != 0){
    qpcr_refseq[i,1] = as.character(conv_table[which(conv_table[,1]==as.character(qpcr[i,3]))[1],2])
  }
  qpcr_refseq[i,2:5] = as.numeric(qpcr[i,12:15]);
}
qpcr_refseq[qpcr_refseq[,1]=="",1] = NA;
qpcr_refseq = qpcr_refseq[!is.na(qpcr_refseq[,1]),]
colnames(qpcr_refseq) = c("transcripts",samples)
RNAseq_index_qPCR = match(qpcr_refseq[,1],row.names(RNAseq[!is.na(glens.RNAseq),]))

#***************************************************
# i) Random datasets 
# randomized cells for each location. Single poisson distribution random dataset
for(i in locations){
  x = read.table(paste0(root, "Raw", gse, i,".txt"),
                 header=T, sep="\t",row.names = 1);
  B = x;
  for(j in 1:ncol(x)){
    Irow = sample(1:nrow(x), nrow(x), replace = FALSE);
    B[,j] = B[Irow,j];
  }
  for(k in 1:nrow(x)){
    Icol = sample(1:ncol(x), ncol(x), replace = FALSE);
    B[k,] = B[k,Icol];
  }
  write.table(B, paste0(root, "Raw", gse, i,"_rand.txt"),
              sep="\t", quote=FALSE);
}

#Poisson distributed random dataset
x <- read.table(paste0(root, "Raw", gse, ilm[1], ".txt"),
                header=T, sep="\t",row.names = 1);

pois = matrix(rpois(ncol(x)*nrow(x),10),ncol=ncol(x),nrow=nrow(x));
colnames(pois) = colnames(x);
rownames(pois) = rownames(x);
write.table(pois, paste0(root, "Raw", gse, ilm[1], "_pois.txt"),
            sep="\t", quote=FALSE)

#***************************************************
# 1) TMM (edgeR) (DONE)
#

library(edgeR)

for (i in datasets){
  print(i);
  x <- read.table(paste0(root, "Raw", gse, i,".txt"),
                  header=T, sep="\t",row.names = 1)
  x = x[!is.na(glens.RNAseq),]
  tmp = {};
  for (j in 1:ncol(x)){
    tmp[j] = strsplit(colnames(x)[j],"_")[[1]][4]
  }
  group <- factor(tmp); rm(tmp);
  y <- DGEList(counts=x,group=group)
  y <- calcNormFactors(y)
  z <- estimateCommonDisp(y)
  m <- z$pseudo.counts
  write.table(m, paste0(root, "TMM-",packageVersion("edgeR"), gse, i,"_TMM-",packageVersion("edgeR"),".txt"),
              sep="\t", quote=FALSE)
}

#******************************************************
# 2) DESeq2 (DONE)
#

library(DESeq2)

for(i in datasets){
  print(i);
  x <- read.table(paste0(root, "Raw", gse, i,".txt"),
                  header=T, sep="\t",row.names = 1)
  x = x[!is.na(glens.RNAseq),]+1
  tmp = {};
  for (j in 1:ncol(x)){
    tmp[j] = strsplit(colnames(x)[j],"_")[[1]][4]
  }
  cond = data.frame(row.names = colnames(x), group = tmp)
  dds = DESeqDataSetFromMatrix(as.matrix(x), colData=cond, design = ~ group)
  dds = estimateSizeFactors(dds)
  y = as.data.frame(counts(dds,normalized=TRUE))
  write.table(y,paste0(root,"DESeq-",packageVersion("DESeq"), gse, i,"_DESeq-",packageVersion("DESeq"),".txt"),
              sep="\t", quote=FALSE);
}


#**********************************************************
# 3) Quantile (DONE)
# normalize.quantiles function derived from Bolstad et al, Bioinformatics (2003)
#

library(preprocessCore)
for(i in datasets){
  print(i)
  x <- read.table(paste0(root, "Raw", gse, i, ".txt"),
                  header=T, sep="\t",row.names = 1);
  x = x[!is.na(glens.RNAseq),]
  y = normalize.quantiles(as.matrix(x),copy=TRUE);
  rownames(y) <- rownames(x, do.NULL = FALSE);
  colnames(y) <- colnames(x, do.NULL = FALSE);
  write.table(y,paste0(root, "quant-",packageVersion("preprocessCore"), gse, i, "_quant-",packageVersion("preprocessCore"),".txt"),
              sep="\t", quote=FALSE);
}

#*********************************************************
# 4) RPKM (DONE)
# RPKM calulation derived from Mortazavi etal, Nature Methods (2008)
# (opimized by KRC) (editted by TSJ 12/2/16)

for(i in datasets){
  print(i);
  x <- read.table(paste0(root, "Raw", gse, i,".txt"),
                  header=T, sep="\t",row.names = 1)
  x = x[!is.na(glens.RNAseq),]
  totalReads = colSums(x) # total reads per sample
  scaledReads <- totalReads/10^9
  corrector <- outer(glens.RNAseq[!is.na(glens.RNAseq)], scaledReads, "*")
  y <- x / corrector
  write.table(y, paste0(root, "RPKM", gse, i, "_RPKM.txt"),
              sep="\t", quote=FALSE)
}

#*********************************************************
# 5) TPM FIXED 7-14-16 8-17-16
# TPM calculation derived from Wagner et al, Theory Biosciences (2012)
# (cleaned up by KRC)

for(i in datasets){
  print(i);
  x <- read.table(paste0(root, "Raw", gse, i,".txt"),
                  header=T, sep="\t",row.names = 1)
  x = x[!is.na(glens.RNAseq),]
  y <-  x / (glens.RNAseq[!is.na(glens.RNAseq)]/1000) # reads per kilobyte
  scaledReads = colSums(y)/1e6;
  y <- sweep(y, 2, scaledReads, "/")
  write.table(y, paste0(root, "TPM", gse, i, "_TPM.txt"),
              sep="\t", quote=FALSE)
}

#**********************************************************
# 6) Log2 (DONE)
# note:  1 added to data matrix to remove -Inf and convert to 0s
#

for(i in datasets){
  print(i);
  x <- read.table(paste0(root, "Raw", gse, i,".txt"),
                  header=T, sep="\t",row.names = 1);
  x = x[!is.na(glens.RNAseq),]
  x = log2(x+1); #KRC: what about the first (gene length) column?
  write.table(x,paste0(root, "Log2", gse, i, "_Log2.txt"),
              sep="\t", quote=FALSE)
}

#*********************************************************************
# Calculating total number of reads from each file
#
if (FALSE){
count_total = 0;
for(method in c("Raw")){
  for(seq_site in c(ilm, lif)){
    print(c(method,seq_site))
    x <- read.table(paste0(root, method, gse, seq_site,"_",method,".txt"),
                    header=T, sep="\t",row.names = 1);
    x[,1] <- NULL;
    y = sum(colSums(x));
    print(y);
    count_total = count_total + y;
  }
}
print(count_total);
}
#***********************************************************************
# Within site linearity 9/8/15, 9/24/15, 10-14-15, 10-15-15, 8-15-16, 12-23-16
#
###KRC: U haven't tried to fix any of te code below here. But the big blocks
### with all the magic are (a) ugly and (b) unreadable and (c) probably an
### inefficient way to do womthing simple.  If you assign row names and column
### names to a data frame (instead of a matrix), then you can access each cell
### by name; e.g.,
###	cor_mn[method, seq_site] <- whatever

cor_mn = matrix(0,length(datasets_noROC),length(methodlist))
colnames(cor_mn) <- methodlist
rownames(cor_mn) <- datasets_noROC;
cor_sd = cor_mn
for(method in methodlist){
  for(seq_site in datasets_noROC){
    print(c(method, seq_site))
    if (method == "Raw"){file_path = paste0(root, method, gse, seq_site, ".txt")}
    else { file_path = paste0(root, method, gse, seq_site, "_", method,".txt")}
    mat <- read.table(file_path, header=T, sep="\t",row.names = 1);
    if (method == "Raw"){mat = mat[!is.na(glens.RNAseq),]}
    tmp = vector();
    for (j in 1:ncol(mat)){
      tmp[j] = strsplit(colnames(mat)[j],"_")[[1]][4]
    }
    tech_rep = as.data.frame(table(tmp))[,2]
    x = c(rep(0,tech_rep[1]),
          rep(0.25,tech_rep[3]),
          rep(0.75, tech_rep[4]),
          rep(1,tech_rep[2]))
    test_mat = cbind(mat[,1:tech_rep[1]],mat[,(sum(tech_rep[1:2])+1):sum(tech_rep[1:3])],
                        mat[,(sum(tech_rep[1:3])+1):sum(tech_rep[1:4])], mat[,(tech_rep[1]+1):sum(tech_rep[1:2])])
    cor_probes = abs(quickCor(test_mat,x))
    final = cor_probes[!is.na(cor_probes)];
    cor_mn[seq_site, method] = mean(final);
    cor_sd[seq_site, method] = sd(final);
  }
}
    
write.table(cor_mn,"~/Desktop/SEQC_norm/Statistics/linearity_mn_12-25-16.txt",
            sep="\t", quote=FALSE);
write.table(cor_sd,"~/Desktop/SEQC_norm/Statistics/linearity_sd_12-25-16.txt",
            sep="\t", quote=FALSE);

#************************************************************
# qPCR comparison 9-12-15, 9-24-15, 10-14-15, 10-15-15, 8-19-16, 12-20-16
#

qPCR_err = array(0,dim=c(length(methodlist),length(datasets_noROC),4,3),
                 dimnames=list(methodlist,datasets_noROC,samples,errors));
for(method in methodlist){
  for(seq_site in datasets_noROC){
    print(c(method, seq_site))
    if (method == "Raw"){file_path = paste0(root, method, gse, seq_site, ".txt")}
    else { file_path = paste0(root, method, gse, seq_site, "_", method,".txt")}
    mat <- read.table(file_path, header=T, sep="\t",row.names = 1);
    if (method == "Raw"){mat = mat[!is.na(glens.RNAseq),]}
    tmp = vector();
    for (j in 1:ncol(mat)){
      tmp[j] = strsplit(colnames(mat)[j],"_")[[1]][4]
    }
    tech_rep = as.data.frame(table(tmp))[,2]
    # code for qPCR RNA-Seq comparison
    for(sample in samples){
      x = qpcr_refseq[,sample];
      y = rowMeans(log2(mat[RNAseq_index_qPCR,tmp==sample]+1))
      x1 = x[x!=0 & y!=0 & !is.na(x) & !is.na(y)]
      y1 = y[x!=0 & y!=0 & !is.na(x) & !is.na(y)]
      fit = lm(x1~y1)
      Yerrs = abs((fit$coefficients[2]*as.numeric(x1)+as.numeric(fit$coefficients[1])) - as.numeric(y1))
      Xerrs = abs((as.numeric(y1)-fit$coefficients[1])/fit$coefficients[2] - as.numeric(x1))
      Perrs = abs(Xerrs * cos(atan(Yerrs / Xerrs)))
      qPCR_err[method,seq_site,sample,"Xerr"] = mean(Xerrs)
      qPCR_err[method,seq_site,sample,"Yerr"] = mean(Yerrs)
      qPCR_err[method,seq_site,sample,"Perr"] = mean(Perrs)
    }
  }
}

save(qPCR_err, file = "~/Desktop/SEQC_norm/Statistics/qPCR_err_5-2-17.RData")
#************************************************************
# USED IN MANUSCRIPT: Between site anova 8-19-16 12-26-16
#

SSsite = matrix(0,sum(!is.na(glens.RNAseq)),length(methodlist),
                dimnames = list(gnames.RNAseq[!is.na(glens.RNAseq)],methodlist))
SSsample = matrix(0,sum(!is.na(glens.RNAseq)),length(methodlist),
                  dimnames = list(gnames.RNAseq[!is.na(glens.RNAseq)],methodlist))
SSresidual = matrix(0,sum(!is.na(glens.RNAseq)),length(methodlist),
                    dimnames = list(gnames.RNAseq[!is.na(glens.RNAseq)],methodlist))
Gmeans = matrix(0,sum(!is.na(glens.RNAseq)),length(methodlist),
                dimnames = list(gnames.RNAseq[!is.na(glens.RNAseq)],methodlist))

for(method in methodlist){
  site = matrix();
  sample = matrix();
  data = matrix();
  for(seq_site in datasets_noROC){
    print(c(method,seq_site))
    if (method == "Raw"){file_path = paste0(root, method, gse, seq_site, ".txt")}
    else { file_path = paste0(root, method, gse, seq_site, "_", method,".txt")}
    mat <- read.table(file_path, header=T, sep="\t",row.names = 1);
    if (method == "Raw"){mat = mat[!is.na(glens.RNAseq),]}
    tmp=c();
    for (j in 1:ncol(mat)){
      tmp[j] = strsplit(colnames(mat)[j],"_")[[1]][4]
    }
    tech_rep = as.data.frame(table(tmp))[,2]
    data = cbind(data,mat[,1:sum(tech_rep[1:4])])
    site = c(site,rep(seq_site,sum(tech_rep[1:4])))
    sample = c(sample,tmp[1:sum(tech_rep[1:4])])
  }
  data = data[,2:dim(data)[2]];
  site = site[2:length(site)];
  sample = sample[2:length(sample)];
  for (gene in gnames.RNAseq[!is.na(glens.RNAseq)]){
    aov.out <- anova(lm(t(data[i,1:dim(data)[2]])~site + sample));
    SSsite[gene,method] = aov.out[[2]][1]
    SSsample[gene,method] = aov.out[[2]][2]
    SSresidual[gene,method] = aov.out[[2]][3]
    Gmeans[gene,method] = mean(as.numeric(data[gene,1:dim(data)[2]]),na.rm=TRUE);
  }
}

write.table(Gmeans,paste0("~/Desktop/SEQC_norm/Statistics/Gmeans_12-27-16.txt"), sep="\t", quote=FALSE);
write.table(SSsite,paste0("~/Desktop/SEQC_norm/Statistics/SSsite_12-27-16.txt"), sep="\t", quote=FALSE);
write.table(SSsample,paste0("~/Desktop/SEQC_norm/Statistics/SSsample_12-27-16.txt"), sep="\t", quote=FALSE);
write.table(SSresidual,paste0("~/Desktop/SEQC_norm/Statistics/SSresidual_12-27-16.txt"), sep="\t", quote=FALSE);

#************************************************************
#  Loading Data
cor_mn = read.table("~/Desktop/SEQC_norm/Statistics/linearity_mn_8-18-16.txt", header=T, sep="\t",row.names = 1);
cor_sd = read.table("~/Desktop/SEQC_norm/Statistics/linearity_sd_8-18-16.txt", header=T, sep="\t",row.names = 1);
qPCR_mn = read.table("~/Desktop/SEQC_norm/Statistics/qPCR_mn_spearman_8-22-16.txt", header=T, sep="\t",row.names = 1);
qPCR_sd = read.table("~/Desktop/SEQC_norm/Statistics/qPCR_sd_spearman_8-22-16.txt", header=T, sep="\t",row.names = 1);
Gmeans = read.table("~/Desktop/SEQC_norm/Statistics/Gmeans_8-19-16.txt", header=T, sep="\t",row.names = 1);
SSsite = read.table("~/Desktop/SEQC_norm/Statistics/SSsite_8-19-16.txt", header=T, sep="\t",row.names = 1);
SSsample = read.table("~/Desktop/SEQC_norm/Statistics/SSsample_8-19-16.txt", header=T, sep="\t",row.names = 1);
SSresidual = read.table("~/Desktop/SEQC_norm/Statistics/SSresidual_8-19-16.txt", header=T, sep="\t",row.names = 1);
method = "Raw"; seq_site = "ILM_AGR";
mat <- read.table(paste0(root, method, gse, seq_site,"_",method,".txt"), header=T, sep="\t",row.names = 1);
mat2 <- read.table(paste0(root, method, gse, seq_site,"_",method,"_qPCR_all.txt"), header=F, sep="\t",row.names = 1);
props = c(rep(0,64),rep(1,64),rep(.25,64),rep(.75,64));

#************************************************************
# USED IN MANUSCRIPT within site linearity plot and qPCR linearity plot
# Generates an example correlation for a sample gene (top) and shows the correlations across all genes and sites (bottom)

pdf("~/Desktop/ws_linear.pdf")
par(mar=c(3.8,4,1,1))
par(mfrow=c(2,1))
plot(props,mat[2,2:257],xaxt='n',ylab="",xlab="")
axis(1,at=c(0,0.25,0.5,0.75,1))
abline(lm(as.numeric(mat[2,2:257])~props), col="black")
title(xlab="Proportion RNA Type B",ylab="Expression Value", line=2.5, cex.lab=1.2)

par(mar=c(4,4,0.1,1))
plot(1:10, seq(0.1,1,0.1), type="n", ylim=c(0,1), ylab="", xlab="",xaxt='n')
axis(1,at=c(1,2,3,4,5,6,7,8,9,10),labels=c(1,2,3,4,5,6,7,8,9,10))
title(xlab="Study Site",ylab="Pearson Correlation", line=2.5, cex.lab=1.2)
lines(1:10,cor_mn[1:10,2], type="l", lwd=1.5, col="blue")
lines(1:10,cor_mn[1:10,3], type="l", lwd=1.5, col="green")
lines(1:10,cor_mn[1:10,4], type="l", lwd=1.5, col="cyan")
lines(1:10,cor_mn[1:10,5], type="l", lwd=1.5, col="purple")
lines(1:10,cor_mn[1:10,6], type="l", lwd=1.5, col="orange")
for (i in 1:9){
  lines(1:10,cor_mn[11:20,i], type="l", lwd=1.5, lty=2, col = "red")
}
lines(1:10,cor_mn[1:10,1], type="l", lwd=1.5, lty=2, col="gray20")
dev.off()

pdf("~/Desktop/qPCR_comp.pdf")
par(mar=c(3.8,4,1,1))
par(mfrow=c(2,1))
i=23
x = c(0,0,.25,.25,.75,.75,1,1)
y=c(-(mat2[i,1]-26.47),log2(mean(as.numeric(mat2[i,5:tech_rep]))),-(mat2[i,3]-30.40),log2(mean(as.numeric(mat2[i,(tech_rep+1):(2*tech_rep)]))),-(mat2[i,4]-26.61),log2(mean(as.numeric(mat2[i,(2*tech_rep+1):(3*tech_rep)]))),-(mat2[i,2]-27.99),log2(mean(as.numeric(mat2[i,(3*tech_rep+1):(4*tech_rep)]))));
plot(x,y,ylab="",xlab="");
abline(lm(y~x),col="black")
title(xlab="Proportion RNA Type B",ylab="Expression Value", line=2.5, cex.lab=1.2)

par(mar=c(4,4,0.1,1))
plot(1:10, seq(0.1,1,0.1), type="n", ylim=c(0,0.5), ylab="", xlab="",xaxt='n')
axis(1,at=c(1,2,3,4,5,6,7,8,9,10),labels=c(1,2,3,4,5,6,7,8,9,10))
title(xlab="Study Site",ylab="Spearman Correlation", line=2.5, cex.lab=1.2)
lines(1:10,qPCR_mn[1:10,2], type="l", lwd=1.5, col="blue")
lines(1:10,qPCR_mn[1:10,3], type="l", lwd=1.5, col="green")
lines(1:10,qPCR_mn[1:10,4], type="l", lwd=1.5, col="cyan")
lines(1:10,qPCR_mn[1:10,5], type="l", lwd=1.5, col="purple")
lines(1:10,qPCR_mn[1:10,6], type="l", lwd=1.5, col="orange")
for (i in 1:9){
  lines(1:10,qPCR_mn[11:20,i], type="l", lwd=1.5, lty=2, col = "red")
}
lines(1:10,qPCR_mn[1:10,i], type="l", lwd=1.5, lty=2, col="gray20")
dev.off()

#******************************************************************
# USED IN MANUSCRIPT inter-site anova plots
#

pdf("~/Desktop/figure3.pdf")
par(mfrow=c(2,2))
pdf("~/Desktop/SSresidual_8-19-16.pdf")
par(mar=c(4,4,1,1))
gg = log2(SSresidual[,1]);
for(i in 1:41534){if(gg[i] < -10){gg[i]=-10}}
plot(lowess(log2(Gmeans[,1]),gg),type="l",xlab="Mean count (log2)",ylab="SSE (log2)",ylim=c(-10,45), lwd=1.5, pin=c(.5,.5),col="gray20")
gg = log2(SSresidual[,2]);
for(i in 1:41534){if(gg[i] < -10){gg[i]=-10}}
lines(lowess(log2(Gmeans[,1]),gg), col="blue", lwd=1.5, lty=1)
gg = log2(SSresidual[,3]);
for(i in 1:41534){if(gg[i] < -10){gg[i]=-10}}
lines(lowess(log2(Gmeans[,1]),gg), col="green", lwd=1.5, lty=1)
gg = log2(SSresidual[,4]);
for(i in 1:41534){if(gg[i] < -10){gg[i]=-10}}
lines(lowess(log2(Gmeans[,1]),gg), col="cyan", lwd=1.5, lty=1)
gg = log2(SSresidual[,5]);
for(i in 1:41534){if(gg[i] < -10){gg[i]=-10}}
lines(lowess(log2(Gmeans[,1]),gg), col="purple", lwd=1.5, lty=1)
gg = log2(SSresidual[,6]);
for(i in 1:41534){if(gg[i] < -10){gg[i]=-10}}
lines(lowess(log2(Gmeans[,1]),gg), col="orange", lwd=1.5, lty=1)
dev.off()

par(mar=c(4,4,1,1))
#pdf("~/Desktop/SSsite_8-19-16.pdf")
gg = log2(SSsite[,1]);
for(i in 1:41534){if(gg[i] < -10){gg[i]=-10}}
plot(lowess(log2(Gmeans[,1]),gg),type="l",xlab="Mean count (log2)",ylab="SSE (log2)",ylim=c(-10,45), lwd=1.5, pin=c(.5,.5), col="gray20") 
gg = log2(SSsite[,2]);
for(i in 1:41534){if(gg[i] < -10){gg[i]=-10}}
lines(lowess(log2(Gmeans[,1]),gg), col="blue", lwd=1.5, lty=1)
gg = log2(SSsite[,3]);
for(i in 1:41534){if(gg[i] < -10){gg[i]=-10}}
lines(lowess(log2(Gmeans[,1]),gg), col="green", lwd=1.5, lty=1)
gg = log2(SSsite[,4]);
for(i in 1:41534){if(gg[i] < -10){gg[i]=-10}}
lines(lowess(log2(Gmeans[,1]),gg), col="cyan", lwd=1.5, lty=1)
gg = log2(SSsite[,5]);
for(i in 1:41534){if(gg[i] < -10){gg[i]=-10}}
lines(lowess(log2(Gmeans[,1]),gg), col="purple", lwd=1.5, lty=1)
gg = log2(SSsite[,6]);
for(i in 1:41534){if(gg[i] < -10){gg[i]=-10}}
lines(lowess(log2(Gmeans[,1]),gg), col="orange", lwd=1.5, lty=1)
#dev.off()

#pdf("~/Desktop/SSsample_8-19-16.pdf")
par(mar=c(4,4,1,1))
gg = log2(SSsample[,1]);
for(i in 1:41534){if(gg[i] < -10){gg[i]=-10}}
plot(lowess(log2(Gmeans[,1]),gg),type="l",xlab="Mean count (log2)",ylab="SSE (log2)",ylim=c(-10, 45), lwd=1.5, pin=c(.5,.5),col="gray20")
gg = log2(SSsample[,2]);
for(i in 1:41534){if(gg[i] < -10){gg[i]=-10}}
lines(lowess(log2(Gmeans[,1]),gg), col="blue", lwd=1.5, lty=1)
gg = log2(SSsample[,3]);
for(i in 1:41534){if(gg[i] < -10){gg[i]=-10}}
lines(lowess(log2(Gmeans[,1]),gg), col="green", lwd=1.5, lty=1)
gg = log2(SSsample[,4]);
for(i in 1:41534){if(gg[i] < -10){gg[i]=-10}}
lines(lowess(log2(Gmeans[,1]),gg), col="cyan", lwd=1.5, lty=1)
gg = log2(SSsample[,5]);
for(i in 1:41534){if(gg[i] < -10){gg[i]=-10}}
lines(lowess(log2(Gmeans[,1]),gg), col="purple", lwd=1.5, lty=1)
gg = log2(SSsample[,6]);
for(i in 1:41534){if(gg[i] < -10){gg[i]=-10}}
lines(lowess(log2(Gmeans[,1]),gg), col="orange", lwd=1.5, lty=1)
#dev.off()

pdf("~/Desktop/SEQC_norm/anova_bar.pdf")
par(mar=c(4,4,1,1))
anova_tbl = rbind(colSums(SSresidual),colSums(SSsample),colSums(SSsite))
anova_tbl = t(t(anova_tbl)/colSums(anova_tbl))*100
xx = rep(0,3)
anova_tbl = matrix(c(anova_tbl[,1],xx,xx,xx,xx,xx,xx,xx,anova_tbl[,2],xx,xx,xx,xx,xx,xx,xx,anova_tbl[,3],xx,xx,xx,xx,xx,xx,xx,anova_tbl[,4],xx,xx,xx,xx,xx,xx,xx,anova_tbl[,5],xx,xx,xx,xx,xx,xx,xx,anova_tbl[,6],xx,xx,xx,xx,xx,xx,xx,anova_tbl[,7]),ncol=7);
col = c(rep("gray20",3),rep("blue",3),rep("green",3),rep("cyan",3),rep("purple",3),rep("orange",3),rep("indianred4",3))
den = c(10,100,30);
barplot(anova_tbl[,1:7],col=col,density=den, names.arg=c("Raw","TMM","DESeq","Quant","RPKM","TPM","Log2"),cex.names=.8, xlab="Normalization method", ylab="Percentage of variance")
dev.off()

library(ggplot2)
library(reshape2)
DF <- melt(qPCR_err)
pdf("~/Desktop/qPCR_err_X.pdf")
ggplot(data=DF, aes(x = Var2, y = value, fill = Var4)) + geom_bar(stat="identity") + facet_grid(~ Var1) + theme(axis.text.x=element_text(angle=90, vjust=0.25, hjust=1, size=3),strip.text.x = element_text(size=5)) + xlab("Site and platform") + ylab("Error");
dev.off()
pdf("~/Desktop/qPCR_err_Y.pdf")
ggplot(data=DF, aes(x = Var1, y = value, fill = Var2)) + geom_bar(stat="identity") + facet_grid(~ Var4) + theme(axis.text.x=element_text(angle=90, vjust=0.25, hjust=1, size=5),strip.text.x = element_text(size=10)) + xlab("Normalization") + ylab("Error");
dev.off()
pdf("~/Desktop/qPCR_err_Z.pdf")
ggplot(data=DF, aes(x = Var4, y = value, fill = Var1)) + geom_bar(stat="identity") + facet_grid(~ Var2) + theme(axis.text.x=element_text(angle=90, vjust=0.25, hjust=1, size=5),strip.text.x = element_text(size=0),legend.text=element_text(size=5))  + xlab("Error type") + ylab("Error");
dev.off()
