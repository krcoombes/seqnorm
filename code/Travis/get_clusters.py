#!/usr/bin/python

import sys
import csv
import numpy

#*********************************************************************
# Used for matching between two files
#
#
#*********************************************************************

#*********************************************************************
# Help menu
if sys.argv[1] == "help" or sys.argv[1] == "h" or sys.argv[1] == "--help" or sys.argv[1] == "-h":
	print "Argument 1 is the first file to match a column"
	print "Argument 2 is the second file to match a column"
	print "Argument 3 is the output file name"
	print "Argument 4 selects the subfunction (always specify cmo)"
	print "Argument 5 is the column in file 1 to match to the specified column in file 2"
	print "Argument 6 is the column in  file 2 to match to the specified column in file 1"
	print "Argument 7 is the first column to print in file 1"
	print "Argument 8 is the last column to print in file 1 such that columns arg7-arg8 from file 1 are printed"
	print "Argument 9 is the first column to print in file 2"
	print "Argument 10 is the last column to print in file 2 such that columns arg9-arg10 from file 2 are printed after arg7-arg8 from file1"
	print "Argument 11 is the delimiter for file 1 (note: always use csv files)"
	print "Argument 12 is the delimiter for file 2 (note: always use csv files)"
	print "Argument 13 is the delimiter for the output file"
else:
	arg1 = sys.argv[1]
	arg2 = sys.argv[2]
	arg3 = sys.argv[3]
	arg4 = str(sys.argv[4])
	arg11 = str(sys.argv[11])
	arg12 = str(sys.argv[12])
	arg13 = str(sys.argv[13])
	
	f = open(arg1, 'rU') 							# Opens first file
	i_1 = csv.reader(f, delimiter=arg11)
	file_1 = list(i_1)
	print arg1;
		
	g = open(arg2, 'rU')							# Opens second file
	i_2 = csv.reader(g, delimiter=arg12)
	file_2 = list(i_2)
	print arg2
	
	fout = open(arg3, 'w')				# Creates output file
	print arg3
	
	#*********************************************************************
	# Below is for matching clusters with Linnarsson data
	
	if arg4 == "mlc":
		print "Matching Linnarson clusters with own clusters from QCM"
		out_matrix = [];
		x = 0;
		while x < len(file_1):
			if(file_1[x][0][0:7] == "Cluster"):
				out_matrix.append(file_1[x][0]);
			y = 0;
			while y < len(file_2[0]):
				z = 0;
				while z < len(file_2):
					if file_1[x][0] == file_2[z][y]:
						out_matrix.append([file_1[x][0], file_2[0][y]]);
					z = z+1;
				y = y+1;
			x = x+1;
	
		out_matrix.append(file_2[0])                        #Whole line second file
		while x < len(file_1):
			y=0;
			while y < len(file_2):
				if file_1[x][1] == file_2[y][1]:
					out_matrix.append(file_2[y])
				y=y+1;
			x=x+1;
	
	#**********************************************************************
	# Below prints matching from functional enrichment
	
	if arg4 == "mci":
		print "Matching elements of functional enrichment"
		out_matrix = [];
		x = 0;
		out_matrix.append(["category","term","Huang_count","Linn_count","Huang_pvalue","Linn_pvalue"])
		while x < len(file_1):
			y=0;
			while y < len(file_2):
				if file_1[x][1] == file_2[y][1]:
					out_matrix.append([file_1[x][0], file_1[x][1], file_1[x][2], file_2[y][2], file_1[x][4], file_2[y][4]])
				y=y+1;
			x=x+1;
	#**********************************************************************
	# Below outputs fields matched by a specified field
	if arg4 == "cmo":
		print "Custom Matching of inputted parameters"
		out_matrix = [];
		f1mat = int(sys.argv[5])
		f2mat = int(sys.argv[6])
		f1outst = int(sys.argv[7])
		if str(sys.argv[8]) == 'end':
			f1outsp = len(file_1[1])+1;
		elif str(sys.argv[8]).isdigit() and int(sys.argv[8]) < len(file_1[1]):
			f1outsp = int(sys.argv[8])
		else:
			print 'Error the file 1 stop column option'
		f2outst = int(sys.argv[9])
		if str(sys.argv[10]) == 'end':
			f2outsp = len(file_2[1])+1;
		elif str(sys.argv[10]).isdigit() and int(sys.argv[10]) < len(file_1[1]):
			f2outsp = int(sys.argv[10])
		else:
			print 'Error the file 2 stop column option'
		f1len = len(file_1)
		f2len = len(file_2)
		x=0;
		fnd = 0;
		while x < f1len:
			y=0;
			fnd = 0;
			while y < f2len and fnd == 0:
				if file_1[x][f1mat].upper().strip() == file_2[y][f2mat].upper().strip():
					fnd = 1
					out_matrix.append(file_1[x][f1outst:f1outsp]+file_2[y][f2outst:f2outsp])
				y = y + 1
			x = x + 1
					
	for item in out_matrix:									# Writes the annotated matrix to the output file
		x = 0;
		while x < len(item):
			if x != len(item)-1:
				fout.write(str(item[x])+arg13);
			else:
				fout.write(str(item[x]));
			x = x + 1;
		fout.write("\n")
